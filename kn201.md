[Табличка](https://docs.google.com/spreadsheets/d/1MdoQXC8-2ZRQys88xQFbkQZzCDEAJOK3FCbDdBMCXME)

| Студент              | Ник           | 01    | 02    | 03   | 04   | 05   | 06   | 07   | 08   | 09   | 10   |  13.06  |  04.07  |
| -------------------- | ------------- | ----- | ----- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ------- | ------- |
| Байдурова Ксения     |  |  |
| Бородина Татьяна     | [Tatyanos](https://gitlab.com/flp-course-2020/Tatyanos)                    | 2? | 1? | 2? | 1? | 1? | | 1? | 3 | | | | + |
| Валеев Руслан        | [Dambldore](https://gitlab.com/flp-course-2020/Dambldore)                  | 2? | 1? | 2? | 1? | 1? |  | 1? |  |  |  |  | + |
| Воронцова Софья      | [sofaviro](https://gitlab.com/flp-course-2020/sofaviro)                    |  |
| Высоковских Данил    | [VysokovskikhDanil](https://gitlab.com/flp-course-2020/VysokovskikhDanil)  |  |
| Давыдова Влада       | [gbegbwltgb](https://gitlab.com/flp-course-2020/gbegbwltgb)                | 2? |  | 1? |
| Данилов Максим       | [DESKTOR](https://gitlab.com/flp-course-2020/DESKTOR)   |
| *Есюнин Никита       | [HikitaYes](https://gitlab.com/flp-course-2020/HikitaYes)                  | 3 | 3 | 3 | 2 | 2 | | | | | | + |
| Исаков Юрий          | [RealVirg](https://gitlab.com/flp-course-2020/RealVirg)                    | 2? |
| Кантимиров Игорь     | [igorkantimirov](https://gitlab.com/flp-course-2020/igorkantimirov)        | 3 | 3 | 1? | | 1? | | 1? | | | | | + |
| Кузнецов Данил       | [leslydev](https://gitlab.com/flp-course-2020/leslydev)                    | 2 | 2 | 2 | 2 | 1? | 1? | | | | | | + |
| Логинова Татьяна     |  |  |
| Лукин Данил          | [dallanik](https://gitlab.com/flp-course-2020/dallanik)                    |  |
| Макаров Константин   | [kamnok](https://gitlab.com/flp-course-2020/kamnok)                        |  |
| Матвеев Станислав    | [coolstorybob](https://gitlab.com/flp-course-2020/coolstorybob)            |  |
| Михайлова Полина     | [MikhailovaPolina](https://gitlab.com/flp-course-2020/MikhailovaPolina)    |  |
| Панков Данил         | [MrTimeChip](https://gitlab.com/flp-course-2020/MrTimeChip)                | 3 | 2 | 3 | 3 | 2 | 2? | 3 | 4 | 2? | | + |
| Никитенков Евгений   | [Akatosh1](https://gitlab.com/flp-course-2020/Akatosh1)                    | 3 | 3 | 3 | 2 | 3 | 2 | 2 | 2 | 3* |  | + |
| Перепелкин Иван      | [haski](https://gitlab.com/flp-course-2020/haski)                          | 3 | 3 | 3 | 3 | 3 | 4 | 2? | 4 | | | + |
| Полупанова Алина     | [polu-lina](https://gitlab.com/flp-course-2020/polu-lina)                  | 3 | 3 | | | 2? | | 2 | | 2 | | ? | + | 
| Серебренникова Ирина | [serebrennikova](https://gitlab.com/flp-course-2020/serebrennikova)        | 3 | 3 | 3 | 2 | 2?? | 2 | 3 | 2 | 3 | | + |
| Тихонов Кирилл       |  |  |
| Трубецких Валерия    | [trubetzkih.lera](https://gitlab.com/flp-course-2020/trubetzkih.lera)      |  |
| Усанина Татьяна      | [tatjana24994](https://gitlab.com/flp-course-2020/tatjana24994)            | 3 | 3 | 3 | 1? |  |  | 2? | 1? | 2? | | + |
| Шишкин Алексей       | [Noppik](https://gitlab.com/flp-course-2020/Noppik)                        | 3 | 3 | 3 | 3 | 3 | 4 | 3 | 4 |  |  | + |
